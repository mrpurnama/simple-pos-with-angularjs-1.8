'use strict';

// import { FontAwesomeModule } from '@fortawesome/fontawesome-free'

// Define the `posApp` module
angular.module('posApp', [
  'ngRoute',
  'core',
  'publicView',
  'productList'
]);
