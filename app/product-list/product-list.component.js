'use strict';

// Register `phoneList` component, along with its associated controller and template
angular.
  module('productList').
  component('productList', {
    templateUrl: 'product-list/product-list.template.html',
    controller: ['$scope', 'Product',
      function ProductListController($scope, Product) {
        this.products = Product.query();
        // this.orderProp = 'age';
        $scope.cart = [];

        var findItemById = function(products, id) {
          // console.log('masik');
          return products.find(function(product) {
            return product.id === id;
          });
        };

        $scope.getCost = function(product) {
          return product.qty * product.price;
        };

        $scope.addItem = function(productToAdd) {
          var found = findItemById($scope.cart, productToAdd.id);
          // console.log(found);
          if (found) {
            found.qty += 1;
          }
          else {
            $scope.cart.push(angular.copy(productToAdd));
          }
        };

        $scope.reduceItem = function(productToReduce) {
          console.log(productToReduce.qty);
          if (productToReduce.qty === 1) {

            console.log('kosong');
            var index = $scope.cart.indexOf(productToReduce);
            $scope.cart.splice(index, 1);
          }
          productToReduce.qty -= 1;
          console.log('tidak kosong');
          console.log(productToReduce.qty);
        };

        $scope.getTotal = function() {
          var total =  $scope.cart.reduce(function(sum, product) {
            return sum + $scope.getCost(product);
          }, 0);
          console.log('total: ' + total);
          return total;
        };

        $scope.clearCart = function() {
          $scope.cart.length = 0;
        };
        
        $scope.removeItem = function(product) {
          var index = $scope.cart.indexOf(product);
          $scope.cart.splice(index, 1);
        };

        $scope.isVisible = false;

        $scope.showCart = function() {
          $scope.isVisible = $scope.isVisible = true;
        };

      }]

  });
