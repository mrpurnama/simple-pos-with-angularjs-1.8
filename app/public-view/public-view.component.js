'use strict';

// Register `header` component, along with its associated controller and template
angular.
  module('publicView').
  component('publicView', {
    templateUrl: 'public-view/public-view.template.html',
    controller: function PublicViewController() {
      this.name = 'Mochamad Rizky P';
      this.role = 'Pemilik';
      this.company = 'CV. ABC';
    }
  });
