'use strict';

// Register `sidebar` component, along with its associated controller and template
angular.
  module('posApp').
  component('sideNav', {
    templateUrl: 'public-view/top-nav.template.html',
    controller: function SideNaveController() {

    }
  });
