'use strict';

// Register `header` component, along with its associated controller and template
angular.
  module('posApp').
  component('topNav', {
    templateUrl: 'public-view/top-nav.template.html',
    controller: function TopNavController() {
      this.name = 'Mochamad Rizky P';
      this.role = 'Pemilik';
      this.company = 'CV. ABC';
    }
  });
