'use strict';

angular.
  module('posApp').
  config(['$routeProvider',
    function config($routeProvider) {
      $routeProvider.
        when('/pos', {
          template: '<public-view></public-view><product-list></product-list>'
        }).
        otherwise('/pos');
    }
  ]);
